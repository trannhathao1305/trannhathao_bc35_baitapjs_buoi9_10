var dsnv = [];

const DSNV = "DSNV";
var dataJson = localStorage.getItem(DSNV);
if(dataJson){
    var dataRaw = JSON.parse(dataJson);
    dsnv = dataRaw.map(function(item){
        return new NhanVien(
            item.taiKhoan,
            item.hoTen,
            item.email,
            item.matKhau,
            item.ngayLam,
            item.luongCoBan,
            item.chucVu,
            item.gioLam,
            item.tongLuong,
            item.xepLoai,
        )
    })
    renderDsnv(dsnv);
};

function saveLocalStorage() {
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV, dsnvJson);
};

// thêm nhân viên
function themNv(){
    var newNv = layThongTinTuForm();
    var isValid = true;
    // kiểm tra tài khoản
    isValid = isValid & kiemTraRong(newNv.taiKhoan, "tbTKNV") && kiemTraMaNv(newNv.taiKhoan, dsnv, "tbTKNV");
    // kiểm tra họ và tên
    // isValid = isValid & kiemTraRong(newNv.hoTen, "tbTen");
    isValid = isValid & kiemTraRong(newNv.hoTen, "tbTen") && kiemTraHoVaTen(newNv.hoTen, "tbTen");
    // kiểm tra email
    isValid = isValid & kiemTraRong(newNv.email, "tbEmail") && kiemTraEmail(newNv.email, "tbEmail");
    // kiểm tra mật khẩu
    isValid = isValid & kiemTraRong(newNv.matKhau, "tbMatKhau") && kiemTraMatKhau(newNv.matKhau, "tbMatKhau");
    // kiểm tra ngày làm
    isValid = isValid & kiemTraRong(newNv.ngayLam, "tbNgay");
    // kiểm tra lương cơ bản
    isValid = isValid & kiemTraRong(newNv.luongCoBan, "tbLuongCB");
    // kiểm tra chức vụ
    isValid = isValid & kiemTraRong(newNv.chucVu, "tbChucVu");
    // kiểm tra giờ làm
    isValid = isValid & kiemTraRong(newNv.gioLam, "tbGiolam");

    if(isValid) {
        dsnv.push(newNv);
        saveLocalStorage();
        renderDsnv(dsnv);
    }
};

// xóa nhân viên
function xoaNv(idNv){
    var index = dsnv.findIndex(function(nv){
        return nv.taiKhoan == idNv;
    });
    if(index == -1){
        return;
    };
    dsnv.splice(index, 1);
    renderDsnv(dsnv);
    saveLocalStorage(); 
};

// sửa thông tin nhân viên
function suaNv(idNv){
    var index = dsnv.findIndex(function(nv){
        return nv.taiKhoan === idNv;
    });
    if(index == -1){
        return;
    };
    var nv = dsnv[index];
    showThongTinLenForm(nv);
    document.getElementById("tknv").disabled = false;
};

// cập nhật thông tin nhân viên
function capNhatNv(){
    var nvEdit = layThongTinTuForm();
    var isValid = true;
    // kiểm tra tài khoản
    isValid = isValid & kiemTraRong(nvEdit.taiKhoan, "tbTKNV");
    // kiểm tra họ và tên
    isValid = isValid & kiemTraRong(nvEdit.hoTen, "tbTen") && kiemTraHoVaTen(nvEdit.hoTen, "tbTen");
    // kiểm tra email
    isValid = isValid & kiemTraRong(nvEdit.email, "tbEmail") && kiemTraEmail(nvEdit.email, "tbEmail");
    // kiểm tra mật khẩu
    isValid = isValid & kiemTraRong(nvEdit.matKhau, "tbMatKhau") && kiemTraMatKhau(nvEdit.matKhau, "tbMatKhau");
    // kiểm tra ngày làm
    isValid = isValid & kiemTraRong(nvEdit.ngayLam, "tbNgay");
    // kiểm tra lương cơ bản
    isValid = isValid & kiemTraRong(nvEdit.luongCoBan, "tbLuongCB");
    // kiểm tra chức vụ
    isValid = isValid & kiemTraRong(nvEdit.chucVu, "tbChucVu");
    // kiểm tra giờ làm
    isValid = isValid & kiemTraRong(nvEdit.gioLam, "tbGiolam");
    if(isValid) {
        var index = dsnv.findIndex(function(nv){
            return nv.taiKhoan == nvEdit.taiKhoan;
        });
        if(index == -1) return;
        dsnv[index] = nvEdit;
        saveLocalStorage();
        renderDsnv(dsnv);
        document.getElementById("tknv").disabled = false;
    }; 
};

// tìm nhân viên xếp Loại, từ khóa tìm kiếm: xuất sắc, khá, giỏi, trung bình
function searchInfo() {
    var valueSearchInput = document.getElementById("searchName").value;
    var nvSearch = dsnv.filter(value => {
        return value.xepLoaiNv().toUpperCase().includes(valueSearchInput.toUpperCase())
    });
    renderDsnv(nvSearch);
};


