// kiểm tra các input bị rỗng
function kiemTraRong(value, idError) {
    if(value.length == 0) {
        document.getElementById(idError).innerText = "Trường hợp này không được để trống !";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    };
};

// kiểm tra mã nhân viên có bị trùng hay không ?
function kiemTraMaNv(idNv, listNv, idError) {
    var index = listNv.findIndex(function(nv){
        return nv.taiKhoan == idNv;
    })
    if(index == -1) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).innerText = "Mã tài khoản đã tồn tại !";
        return false;
    };
};

// kiểm tra email hợp lệ
function kiemTraEmail(value, idError) {
    const re =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = re.test(value);
    if(!isEmail) {
        document.getElementById(idError).innerText = "Email không hợp lệ !";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    };
};

// kiểm tra tài khoản có từ 4 -> 6 ký số và ko có chữ số
function onlyNumbers(text) {
    text = text.replace(/[^0-9]/g, '');
    var inputResult = document.getElementById("tknv");
    inputResult.value = text;
};

// kiểm tra mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)
function kiemTraMatKhau(value, idError) {
    var re = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,10}$/;
    var isPassWord = re.test(value);
    if(!isPassWord) {
        document.getElementById(idError).innerText = "Mật khẩu không hợp lệ !";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    };
};

// kiểm tra họ và tên
function kiemTraHoVaTen(value, idError) {
    var re = /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    var isPassWord = re.test(value);
    if(!isPassWord) {
        document.getElementById(idError).innerText = "Họ và tên không hợp lệ!";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    };
};

// tự format tiền khi user đang nhập tiền vào ô input
// $(document).on("keypress", ".just-number", function (e) {
//     let charCode = (e.which) ? e.which : e.keyCode;
//     if (charCode > 31 && (charCode < 48 || charCode > 57)) {
//       return false;
//     }
// });
// $(document).on('keyup', '.price-format-input', function (e) {
//     let val = this.value;
//     val = val.replace(/,/g, "");
//     if (val.length > 3) {
//         let noCommas = Math.ceil(val.length / 3) - 1;
//         let remain = val.length - (noCommas * 3);
//         let newVal = [];
//         for (let i = 0; i < noCommas; i++) {
//             newVal.unshift(val.substr(val.length - (i * 3) - 3, 3));
//         }
//         newVal.unshift(val.substr(0, remain));
//         this.value = newVal;
//     } else {
//         this.value = val;
//     }
// });
// $(document).ready(function(){
//     $('#luongCB').focus();
// });


