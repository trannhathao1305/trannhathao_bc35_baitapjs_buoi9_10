function NhanVien(
    _taiKhoan, _hoTen, _email, _matKhau, _ngayLam,
    _luongCoBan, _chucVu, _gioLam, _tongLuong, _xepLoai
) {
    this.taiKhoan = _taiKhoan;
    this.hoTen = _hoTen;
    this.email = _email;
    this.matKhau = _matKhau;
    this.ngayLam = _ngayLam;
    this.luongCoBan = _luongCoBan;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;
    this.tongLuong = _tongLuong;
    this.xepLoai = _xepLoai;

    this.tinhTongLuong = function() {
        if(_chucVu == "Giám đốc"){
            var tongLuong = (this.luongCoBan * 3);
            return tongLuong.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
        } else if(_chucVu == "Trưởng phòng") {
            var tongLuong = (this.luongCoBan * 2);
            return tongLuong.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
        } else if (_chucVu == "Nhân viên") {
            var tongLuong = (this.luongCoBan * 1);
            return tongLuong.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
        }
    }

    this.xepLoaiNv = function() {
        if(_gioLam >= 192){
            return "Nhân viên xuất sắc";
        } else if(_gioLam >= 176){
            return "Nhân viên giỏi";
        } else if(_gioLam >= 160){
            return "Nhân viên khá";
        } else if(_gioLam < 160){
            return "Nhân viên trung bình";
        }
    }
    
};